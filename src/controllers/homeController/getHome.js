import route from 'Utilities/route'

export function get (loggedIn) {
  let address = loggedIn ? 'home' : 'login'
  route(address)
}
