import * as d3 from 'd3'

export default function draw (data, width, height, key) {
  const margin = { top: 10, right: 20, bottom: 20, left: 40 }
  const marginSums = {
    horizontal: margin.left + margin.right,
    vertical: margin.top + margin.bottom
  }

  width -= marginSums.horizontal
  height -= marginSums.vertical

  let svg = d3.select('#chartSvg')
    .attr('width', width + marginSums.horizontal)
    .attr('height', height + marginSums.vertical)

  svg.select('g').remove()

  let g = svg.append('g')
    .attr('transform', `translate(${margin.left}, ${margin.top})`)

  g.append('g')
    .append('text')
    .text(`${key.charAt(0).toUpperCase() + key.slice(1)} Chart`)
    .attr('x', width / 2)
    .attr('y', 5)
    .classed('title', true)

  console.log(data)

  let x = d3.scaleTime()
    .domain(d3.extent(data, d => d.date))
    .range([0, width])

  let y = d3.scaleLinear()
    .domain([0, d3.max(data, d => d[key])])
    .range([height, 0])

  let xAxis = d3.axisBottom(x)
  let yAxis = d3.axisLeft(y)

  let line = d3.line()
    .x(d => x(d.date))
    .y(d => y(d[key]))

  g.append('path')
    .datum(data)
    .attr('d', line)
    .classed('line', true)

  g.append('g')
    .selectAll('circle')
    .data(data)
    .enter()
    .append('circle')
    .attr('cx', d => x(d.date))
    .attr('cy', d => y(d[key]))
    .attr('r', 5)
    .classed('datapoint', true)

  g.append('g')
    .call(xAxis)
    .attr('transform', `translate(0, ${height})`)

  g.append('g')
    .call(yAxis)

  let yTicks = y.ticks().filter(a => a !== 0)

  g.append('g')
    .selectAll('line')
    .data(yTicks)
    .enter()
    .append('line')
    .attr('x2', width)
    .attr('y1', d => y(d) + 0.5)
    .attr('y2', d => y(d) + 0.5)
    .classed('tickExtension', true)

  function drawAxes () {

  }
}
