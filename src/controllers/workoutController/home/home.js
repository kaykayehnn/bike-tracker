import * as d3 from 'd3'

import * as Workout from 'Models/Workout'
import homeTemplate from 'Views/home.hbs'
import { getData, formats, setDefaults } from './formLogic'
import formRenderer from './form'
import drawChart from './chart'

import showView from 'Utilities/showView'
import debounce from 'Utilities/debounce'
import * as storage from 'Utilities/storage'
import { showNotification } from 'Config/notifications'

const RESIZE_DELAY = 100

const format = '%Y-%m-%d %H:%M'
const dateFormat = d3.timeFormat(format)
const dateParse = d3.timeParse(format)

let displayKey = 'calories'

export function getHome () {
  let context = { datetime: dateFormat(new Date()) }
  let html = homeTemplate(context)
  showView(html)

  let drawFn = formRenderer()
  let formDiv = document.getElementById('workoutForm')
  let withDimensions = () => drawFn(formDiv.offsetWidth * 3 / 4, formDiv.offsetHeight)

  withDimensions()
  window.addEventListener('resize', debounce(withDimensions, RESIZE_DELAY), true)

  let submit = document.getElementById('addWorkoutBtn')
  submit.addEventListener('click', submitWorkout)

  let chartDiv = document.getElementById('chart')

  let id = storage.get('id')
  Workout.getMyWorkouts(id)
    .then(res => {
      res.data = parseData(res.data)
      let withData = () => renderResponsiveChart(chartDiv, res.data)

      withData()
      window.addEventListener('resize', debounce(withData, RESIZE_DELAY), true)
    })
    .catch(err => console.log(err))
}

function submitWorkout (e) {
  e.preventDefault()
  let data = getData()

  for (let k in data) {
    let fn = formats[k + 'Fn']
    data[k] = fn ? +fn(data[k]) : data[k]
  }
  data.date = dateParse(data.date)

  let keysToValidate = ['duration', 'distance', 'calories', 'date']
  for (let i = 0; i < keysToValidate.length; i++) {
    let k = keysToValidate[i]
    if (!data[k]) {
      showNotification(`Invalid ${k}`)
      return
    }
  }

  Workout.addWorkout(data)
    .then(res => {
      showNotification('Workout successfully added.')

      setDefaults()
      getHome()
    })
}

function parseData (data) {
  return data.map(d => {
    d.date = new Date(d.date)
    return d
  })
}

function renderResponsiveChart (container, data) {
  let bounds = container.getBoundingClientRect()
  console.log(bounds)

  drawChart(data, bounds.width, window.innerHeight - bounds.top - 20, displayKey)
}
