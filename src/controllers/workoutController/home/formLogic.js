import * as d3 from 'd3'

export const formats = formatFactory()

const SCALE_CONFIG = {
  duration: {
    format: formats.duration,
    revolution: 1800
  },
  distance: {
    format: formats.distance,
    revolution: 10
  },
  calories: {
    format: formats.calories,
    revolution: 250
  }
}

let state

setDefaults()
export function setDefaults () {
  state = {
    duration: {
      revolutions: 0,
      tan: 0,
      quadrant: 0,
      value: 0
    },
    distance: {
      revolutions: 0,
      tan: 0,
      quadrant: 0,
      value: 0
    },
    calories: {
      revolutions: 0,
      tan: 0,
      quadrant: 0,
      value: 0
    }
  }
}

export function dragFactory (radius, d, node, textNode) {
  let bhvr = {}
  let updatePosition = setPosition(radius, textNode).bind(node, d)

  bhvr.onDrag = function onDrag (d) {
    let x = d3.event.x
    let y = d3.event.y

    let currentState = state[d]
    let prevQuadrant = currentState.quadrant
    let quadrant = getQuadrant(x, y, prevQuadrant)
    let tan = Math.abs(x / y)
    if (prevQuadrant === 2 && quadrant === 1) currentState.revolutions++
    else if (prevQuadrant <= 1 && quadrant === 2) {
      if (currentState.value > SCALE_CONFIG[d].revolution) currentState.revolutions--
      else {
        quadrant = 0
        tan = 0
      }
    }

    currentState.quadrant = quadrant
    currentState.tan = tan

    updatePosition()
  }

  bhvr.setPosition = updatePosition

  return bhvr
}

export function getData () {
  let obj = {}
  for (let k in state) {
    obj[k] = state[k].value
  }

  obj.date = document.getElementById('datetime').value
  return obj
}

const ONE_REVOLUTION = Math.PI * 2

let degreeScale = d3.scaleLinear()
  .domain([0, ONE_REVOLUTION])

function setPosition (radius, textNode) {
  return function (d) {
    let currentState = state[d]
    let quadrant = currentState.quadrant
    let tan = currentState.tan

    // x^2 + y^2 = r^2
    // tan = x / y
    // y = x / tan
    // x^2 + (x/tan)^2 = r^2
    // x^2 +x^2/tan^2 = r^2
    // x^2(tan^2 + 1) / tan^2 = r^2
    // x^2(tan^2 + 1) = (r*tan)^2
    // x^2 = (r*tan)^2/(tan^2+1)
    // x = sqrt((r*tan)^2/(tan^2+1))
    let x = Math.sqrt((radius * tan) ** 2 / (tan ** 2 + 1))
    let y = (x / tan) || 0

    let angle = Math.atan(tan)
    // clockwise order
    if (quadrant === 0) { // default
      y = -radius
    } else if (quadrant === 1) {
      y = -y
    } else if (quadrant === 4) {
      angle = Math.PI - angle
    } else if (quadrant === 5) {
      angle = Math.PI
      y = +radius
    } else if (quadrant === 3) {
      x = -x
      angle += Math.PI
    } else if (quadrant === 2) {
      x = -x
      y = -y
      angle = Math.PI * 2 - angle
    }

    let config = SCALE_CONFIG[d]
    let value = (degreeScale(angle) + currentState.revolutions) * config.revolution

    if (value < 0) {
      value = 0
      x = 0
      y = -radius
      currentState.quadrant = 0
    }

    currentState.value = value
    let formatted = `${d.charAt(0).toUpperCase() + d.slice(1)}: ${config.format(value)}`

    d3.select(this)
      .attr('cx', x)
      .attr('cy', y)

    d3.select(textNode)
      .text(formatted)
  }
}

//     -y
//      0
//    2 | 1
// -x ——————— +x
//    3 | 4
//      5
//     +y
function getQuadrant (x, y, prevQuadrant) {
  if (x > 0 && y < 0) return 1
  if (x < 0 && y < 0) return 2
  if (x < 0 && y > 0) return 3
  if (x > 0 && y > 0) return 4
  if (prevQuadrant <= 2) return 0
  if (prevQuadrant >= 3) return 5
}

function formatFactory () {
  let obj = {}

  obj.durationFn = d3.format('d')
  obj.duration = function formatDuration (seconds) {
    let hours = Math.floor(seconds / 3600)
    let minutes = Math.floor(seconds / 60) % 60
    seconds = Math.floor(seconds) % 60

    let str = `${padZero(hours)}:${padZero(minutes)}:${padZero(seconds)}`
    str = str.replace(/^(00:)/, '')

    return str
    function padZero (n) {
      return ('0' + n).slice(-2)
    }
  }

  obj.distanceFn = d3.format('.2f')
  obj.distance = function formatDistance (d) {
    return obj.distanceFn(d) + ' km'
  }

  obj.caloriesFn = d3.format('d')
  obj.calories = function formatCalories (d) {
    return obj.caloriesFn(d) + ' cal'
  }

  return obj
}
