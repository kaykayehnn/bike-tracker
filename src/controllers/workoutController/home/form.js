import * as d3 from 'd3'

import { dragFactory } from './formLogic'
import { getMerge } from 'Utilities/d3'

const COLUMN_COUNT = 3

export default function formRenderer () {
  return function (width, height) {
    const columnWidth = width / COLUMN_COUNT
    const centerY = height / 2
    const centerX = columnWidth / 2

    console.log(width, height)
    let metrics = ['duration', 'distance', 'calories']

    let svg = d3.select('#formSvg')
      .attr('width', width)
      .attr('height', height)

    let gs = svg.selectAll('g')
      .data(metrics)

    gs = getMerge(gs, 'g')

    gs
      .attr('transform', (d, i) => `translate(${i * columnWidth + centerX}, ${centerY})`)
      .each(function (p, j) {
        let g = d3.select(this)
        let dataArr = [p]
        let radius = Math.min(centerX, centerY) * 0.9

        let circle = g.selectAll('circle.big')
          .data(dataArr)

        circle = getMerge(circle, 'circle')

        circle
          .attr('r', radius)
          .attr('stroke', '#98CA32')
          .attr('stroke-width', '3px')
          .attr('fill', 'none')
          .classed('big', true)

        let text = g.selectAll('text')
          .data(dataArr)

        text = getMerge(text, 'text')

        text
          .attr('id', d => `${d}Text`)
          .attr('text-anchor', 'middle')
          .attr('dy', '0.32em')

        let smallCircle = g.selectAll('circle.small')
          .data(dataArr)

        smallCircle = getMerge(smallCircle, 'circle')

        let textNode = text.node()
        let rotatorNode = smallCircle.node()
        let dragBhvr = dragFactory(radius, p, rotatorNode, textNode)

        smallCircle
          .attr('r', 10)
          .attr('fill', '#98CA32')
          .classed('small', true)
          .each(dragBhvr.setPosition)
          .call(d3.drag()
            .on('drag', dragBhvr.onDrag))
      })
  }
}
