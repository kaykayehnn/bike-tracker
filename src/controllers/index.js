import * as homeController from './homeController/'
import * as userController from './userController/'
import * as workoutController from './workoutController/'

export {
  homeController as home,
  userController as user,
  workoutController as workout
}
