export default function validate (username, password, repeat) {
  if (username.length < 5) return 'Username must not be shorter than 5 characters'
  else if (password.length < 8) return 'Password must be at least 8 characters long'
  else if (repeat != null && password !== repeat) return 'Passwords must match'
}
