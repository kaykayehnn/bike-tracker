import validate from '../validate'
import * as User from 'Models/User'
import loginTemplate from 'Views/login.hbs'

import route from 'Utilities/route'
import showView from 'Utilities/showView'
import * as storage from 'Utilities/storage'
import { showNotification } from 'Config/notifications'

export function getLogin () {
  let html = loginTemplate()

  showView(html)

  document.getElementById('loginBtn')
    .addEventListener('click', tryLogin)
}

function tryLogin (e) {
  e.preventDefault()
  let inputs = document.querySelectorAll('#loginForm > input')
  let username = inputs[0].value
  let password = inputs[1].value

  let err = validate(username, password)
  if (err) {
    showNotification(err)
    return
  }

  User.logIn(username, password)
    .then(res => {
      showNotification('Login successful.')

      storage.saveSession(res.data)
      route('')
    }).catch(err => showNotification(err.message))
}
