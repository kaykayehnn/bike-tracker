import * as User from 'Models/User'

import route from 'Utilities/route'
import * as storage from 'Utilities/storage'
import { showNotification } from 'Config/notifications'

export function logout () {
  User.logOut(storage.get('authtoken'))
    .then(res => {
      showNotification('Logout successful.')
      storage.clear()
      route('')
    }).catch(err => showNotification(err.message))
}
