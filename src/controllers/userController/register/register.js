import validate from '../validate'
import * as User from 'Models/User'
import registerTemplate from 'Views/register.hbs'

import route from 'Utilities/route'
import showView from 'Utilities/showView'
import * as storage from 'Utilities/storage'
import { showNotification } from 'Config/notifications'

export function getRegister () {
  let html = registerTemplate()

  showView(html)

  document.getElementById('registerBtn')
    .addEventListener('click', tryRegister)
}

function tryRegister (e) {
  e.preventDefault()
  let inputs = document.querySelectorAll('#registerForm > input')
  let username = inputs[0].value
  let password = inputs[1].value
  let repeat = inputs[2].value

  let err = validate(username, password, repeat)

  if (err) {
    showNotification(err)
    return
  }

  User.register(username, password)
    .then(res => {
      showNotification('User registration successful')

      storage.saveSession(res.data)
      route('')
    }).catch(err => showNotification(err.message))
}
