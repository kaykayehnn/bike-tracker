import axios from 'Utilities/requester'

const myWorkoutsURLTemplate = `/appdata/${APP_KEY}/workouts` +
  '?query={"_acl.creator":"*"}&sort={"date":-1}'

export function getMyWorkouts (id) {
  let url = myWorkoutsURLTemplate.replace('*', id)

  return axios.get(url)
}

export function addWorkout ({ duration, distance, calories, date }) {
  let data = { duration, distance, calories, date }
  return axios.post(`/appdata/${APP_KEY}/workouts`, data)
}
