import '../style/site.css'
import '../style/menu.css'

import route from 'Utilities/route'
import { showNotification } from './notifications'
import * as storage from 'Utilities/storage'
import * as controllers from 'Controllers/index'

const handlerMap = [
  {
    test: /^#?$/,
    handler: controllers.home.get
  },
  {
    test: /^#login$/,
    handler: controllers.user.getLogin
  },
  {
    test: /^#register$/,
    handler: controllers.user.getRegister
  },
  {
    test: /^#logout$/,
    handler: controllers.user.logout,
    authRequired: true
  },
  {
    test: /^#home$/,
    handler: controllers.workout.getHome,
    authRequired: true
  }
]

window.addEventListener('hashchange', router, false)

export default function router () {
  let hash = window.location.hash
  let loggedIn = !!storage.get('authtoken')
  console.log(hash)
  for (let i = 0; i < handlerMap.length; i++) {
    let obj = handlerMap[i]
    let test = obj.test
    if (test.test(hash)) {
      if (test.authRequired) {
        showNotification('You need to be logged in to do this')
        return void controllers.user.getLogin(loggedIn)
      }

      return void obj.handler(loggedIn)
    }
  }

  showNotification('Invalid URL')
  route('')
}
