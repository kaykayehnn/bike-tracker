// prone to earlier hides, not tested well
import '../style/notifications.css'

const HIDE_INTERVAL = 3000
const PULSE_DURATION = 500

let DOMNodes
let hideTimeouts = []
let hideAccessors = (ix) => ({
  get: () => hideTimeouts[ix],
  set: (val) => { hideTimeouts[ix] = val }
})
let pulseTimeouts = []
let pulseAccessors = (ix) => ({
  get: () => pulseTimeouts[ix],
  set: (val) => { pulseTimeouts[ix] = val }
})

function toggle (message, show, level) {
  DOMNodes = DOMNodes || getAndAssignEvents()

  let ix = level || 0
  let DOMNode = DOMNodes[ix]
  let visible = DOMNode.style.display === 'block'
  let accessors = hideAccessors(ix)
  if (show === true || show === null) {
    if (visible) pulse(ix)
    DOMNode.style.display = 'block'

    removeTimeout(accessors)
    hideTimeouts[ix] = setTimeout(() => {
      toggle(null, false, ix)
      removeTimeout(accessors)
    }, HIDE_INTERVAL)
  } else {
    DOMNode.style.display = 'none'
    DOMNode.classList.remove('pulse')
    removeTimeout(accessors)
  }

  DOMNode.innerHTML = `<p>${message}</p>`
}

export function showNotification (message, level) {
  toggle(message, true, level)
}

export function hideLoadingNotification () {
  toggle(null, false, 1)
}

function getAndAssignEvents () {
  let nodes = document.querySelectorAll('.notification')
  for (let i = 0; i < nodes.length; i++) {
    nodes[i].addEventListener('click', () => {
      toggle(null, false, i)
      removeTimeout(hideAccessors(i))
    })
  }
  return nodes
}

function pulse (ix) {
  if (pulseTimeouts[ix] != null) return
  let accessors = pulseAccessors(ix)

  DOMNodes[ix].classList.add('pulse')

  removeTimeout(accessors)
  setTimeout(() => {
    DOMNodes[ix].classList.toggle('pulse', false)
    removeTimeout(accessors)
  }, PULSE_DURATION)
}

function removeTimeout (accessorFns) {
  clearTimeout(accessorFns.get())
  accessorFns.set(null)
}
