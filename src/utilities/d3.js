export function getMerge (selection, element) {
  return selection.merge(selection.enter()
    .append(element))
}
