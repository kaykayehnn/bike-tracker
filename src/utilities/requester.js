import axios from 'axios'
import * as storage from './storage'
import { showNotification, hideLoadingNotification } from 'Config/notifications'

const base64 = window.btoa(`${APP_KEY}:${APP_SECRET}`)
const LOADING_LEVEL = 1

const instance = axios.create({
  baseURL: 'https://baas.kinvey.com/',
  transformRequest: [(data, headers) => {
    let auth = storage.get('authtoken')
    headers['Authorization'] = auth ? `Kinvey ${auth}` : `Basic ${base64}`

    showNotification('Loading...', LOADING_LEVEL)
    return JSON.stringify(data)
  }],
  transformResponse: [(data) => {
    hideLoadingNotification()
    return JSON.parse(data || JSON.stringify(''))
  }]
})

instance.defaults.headers['Content-Type'] = 'application/json'

export default instance
