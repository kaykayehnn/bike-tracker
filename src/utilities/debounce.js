export default function debounce (fn, delay) {
  let timeout
  return function debounced () {
    clearTimeout(timeout)
    timeout = setTimeout(fn, delay)
  }
}
